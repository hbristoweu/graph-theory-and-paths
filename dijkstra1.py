#!/usr/bin/env python3

import sys

from WeightedGraph import Graph, Vertex
from collections import OrderedDict

INF = sys.maxsize # Infinity

def _usfca_example_graph():
  g = Graph()

  g.add_vertex('0')
  g.add_vertex('1')
  g.add_vertex('2')
  g.add_vertex('3')
  g.add_vertex('4')
  g.add_vertex('5')
  g.add_vertex('6')
  g.add_vertex('7')

  g.add_edge('0','1',3)
  g.add_edge('0','2',8)
  g.add_edge('0','3',6)
  g.add_edge('0','4',3)
  g.add_edge('1','5',3)
  g.add_edge('1','6',5)
  g.add_edge('3','7',4)
  g.add_edge('4','7',1)
  g.add_edge('5','6',7)

  return g

def _wikipedia_dijkstra_example():
  g = Graph()
  
  g.add_vertex('A')
  g.add_vertex('B')
  g.add_vertex('C')
  g.add_vertex('D')
  g.add_vertex('E')
  g.add_vertex('F')

  g.add_edge('A', 'B', 7)
  g.add_edge('A', 'C', 9)
  g.add_edge('A', 'F', 14)
  g.add_edge('B', 'C', 10)
  g.add_edge('B', 'D', 15)
  g.add_edge('C', 'D', 11)
  g.add_edge('C', 'F', 2)
  g.add_edge('D', 'E', 6)
  g.add_edge('E', 'F', 9)

  return g


   
def dijkstra(G,src,end=None):
  
  # Create Vertex set Q
  Q = []
  S = []
  dist = {}
  prev = {}
  
  for v in G:
    if str(v) != src:
      dist[v] = INF
      prev[v] = None
    else:
      dist[v] = 0
    Q.append(v)

  while Q:
    # v_ld = smallest dist[v] for v in Q
    v_ld, ld = None, INF
    for v in Q:
      if dist[v] < ld:
        v_ld, ld = v, dist[v]
    
    # If end vertex is defined && if v_ld == end
    if end and str(v_ld) == end:
      try:
        while prev[v_ld]:	# while raises on KeyError instead of breaking (i.e. 'while prev[v_ld] is defined'), gotta try around it
          S = [v_ld] + S
          v_ld = prev[v_ld]
      except KeyError:
        S = [v_ld] + S
      break
  
    # Remove v_ld from Q
    Q.remove(v_ld)

    for v in v_ld.get_connections():
      alt = dist[v_ld] + v_ld.get_weight(v)
      
      if alt < dist[v]:	# A shorter path is discovered
        dist[v] = alt
        prev[v] = v_ld

  print("SPF: src -> end  = ", list(map(lambda x: str(x),S)))
  print(list(map(lambda x: str(x),prev)))

  return (dist, prev)



def main():
  g = _wikipedia_dijkstra_example()
  dijkstra(g,'A','E')
  #g = _usfca_example_graph()
  #dijkstra(g,'0','7')

if __name__ == "__main__":
  main()

