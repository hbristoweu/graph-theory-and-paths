#!/usr/bin/env python3
from sys import stdout
from WeightedGraph import Graph, Vertex

def _wikipedia_dijkstra_example():
  g = Graph()

  g.add_vertex('A')
  g.add_vertex('B')
  g.add_vertex('C')
  g.add_vertex('D')
  g.add_vertex('E')
  g.add_vertex('F')

  g.add_edge('A', 'B', 7)
  g.add_edge('A', 'C', 9)
  g.add_edge('A', 'F', 14)
  g.add_edge('B', 'C', 10)
  g.add_edge('B', 'D', 15)
  g.add_edge('C', 'D', 11)
  g.add_edge('C', 'F', 2)
  g.add_edge('D', 'E', 6)
  g.add_edge('E', 'F', 9)

  return g

def wlt(L):
  for i in L:
    stdout.write(str(i)+'\t')

def warshall2(G):
  vtx_list = []
  for v in G: vtx_list.append(v)
  vtx_count     = len(vtx_list)

  E = G.get_edges()

  R = {}

  for ei in E:
    for ej in E:
      R[str(ei)+str(ej)] = ej[2]	# Evil Python dict voodoo

  stdout.write("  ")
  for ei in E:
    stdout.write(str(ei[0]) + "  ")

def warshall(G):
  # Build Adjacency Matrix
  vtx_list = []
  for v in G: vtx_list.append(v)
  vtx_count	= len(vtx_list)

  R = [[0 for x in range(vtx_count)] for y in range(vtx_count)]

  #print(vtx_count, "|", list(map(lambda x: str(x), vtx_list)))

  # ln 2, 3
  #for i in range(vtx_count): R[i][i] = False	# Redundant? See def

  E = G.get_edges()

  # ln 4, 5	- False or edge weight
  for vi in range(vtx_count):
    for vj in range(vtx_count):
      if G.edge_exists(vtx_list[vi], vtx_list[vj]):
        R[vi][vj] = G.get_edge_weight(vtx_list[vi], vtx_list[vj])

  # Print a table
  stdout.write("\t")
  for v in vtx_list:
    stdout.write(str(v)+'\t')
  print('  ')
  for vi in range(vtx_count):
    stdout.write(str(vtx_list[vi]) + "\t")
    wlt(list(R[vi]))
    stdout.write('\n')

  print('')

  # ln 6+
  for k in range(vtx_count):
    for vi in range(vtx_count):
      for vj in range(vtx_count):
        if R[vi][vj] > (R[vi][k] + R[k][vj]):
          print(R[vi][vj], " > ", R[vi][k], " + ", R[k][vj] )
          R[vi][vj] = (R[vi][k] + R[k][vj])

  # Print a table
  stdout.write("\t")
  for v in vtx_list:
    stdout.write(str(v)+'\t')
  print('  ')
  for vi in range(vtx_count):
    stdout.write(str(vtx_list[vi]) + "\t")
    wlt(list(R[vi]))
    stdout.write('\n')

  return R





def main():
  G = _wikipedia_dijkstra_example()
  r = warshall(G)
  #r = warshall2(G)

if __name__ == "__main__":
  main()
