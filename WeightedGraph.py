class Vertex:
  def __init__(self, node):
    self.id = node
    self.adjacent = {}

  def __str__(self):
    return str(self.id)

  def add_neighbor(self, neighbor, weight=0):
    self.adjacent[neighbor] = weight

  # Returns list of str
  def get_adjacencies(self):
    return [x.id for x in self.adjacent]

  # Returns list of vertex obj
  def get_connections(self):
    return self.adjacent.keys()

  def get_id(self):
    return self.id

  def get_weight(self, neighbor):
    try:
      r = self.adjacent[neighbor]
      return r
    except KeyError:
      return None

class Graph:
  def __init__(self):
    self.vert_dict = {}
    self.num_vertices = 0
    self.diedge = []

  def __iter__(self):
    return iter(self.vert_dict.values())

  def add_vertex(self, node):
    self.num_vertices = self.num_vertices + 1
    new_vertex = Vertex(node)
    self.vert_dict[node] = new_vertex
    return new_vertex

  def get_vertex(self, n):
    if n in self.vert_dict:
      return self.vert_dict[n]
    else:
      return None

  def add_edge(self, frm, to, cost = 0):
    if frm not in self.vert_dict:
      self.add_vertex(frm)
    if to not in self.vert_dict:
      self.add_vertex(to)

    self.vert_dict[frm].add_neighbor(self.vert_dict[to], cost)
    self.vert_dict[to].add_neighbor(self.vert_dict[frm], cost)

    # INSERT REDUNDANCY CHECK ASAP
    self.diedge.append((self.get_vertex(frm), self.get_vertex(to), cost))

  def get_edges(self):
    return self.diedge

  def edge_exists(self, v1, v2):
    for e in self.diedge:
      if (e[0],e[1])==(v1,v2) or (e[0],e[1])==(v1,v2):
        return True
    return False

  def get_edge_weight(self, v1, v2):
    for e in self.diedge:
      if (e[0],e[1])==(v1,v2) or (e[0],e[1])==(v1,v2):
        return e[2]

  def get_vertices(self):
    return self.vert_dict.keys()

